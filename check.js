'use strict';
function check(){
    this.direction = function (main_CF, temp_CF){
        this.main = main_CF;
        this.temp = temp_CF;
        return this.main > this.temp;
    }
}
module.exports = check;
'use strict';
let fs = require('fs');
let Check = require('./check'); //подключение модулей с функциями
let Cf = require('./CF');
let Mine = require('./minecraft');
let mine_func, CF_func, check_func;
let a_1 = 7, b_2 = 3; //измененные параметры, которые требуется найти
let a_1_Point = [], b_2_Point = []; // массив для отображения поиска параметров
let h = 0.1; //шаг
a_1_Point.push(a_1);
b_2_Point.push(b_2);
mine_func = new Mine(); //обознаяение функций
CF_func = new Cf();
check_func = new Check();
let CF; // целевая функция
let prev_dir = 0; let i = 0;
while (i < 150){
    CF = CF_func.CFU(mine_func.minecraft(a_1, b_2)); // подсчет целевой функции
        if (check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1 + h, b_2)))) { // проверка истинности, целевая функция должна становиться меньше
            if ((check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1 + 3 * h, b_2)))) && prev_dir === 1) { // проверка на регулировку шага
                a_1 += h * 3; // если условие выполняется не в первый раз
            } else {
                a_1 += h;
                prev_dir = 1;
            }
        } else if (check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1 - h, b_2)))) {
            if ((check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1 - 3 * h, b_2)))) && prev_dir === 2) {
                a_1 -= h * 3;
            } else {
                a_1 -= h;
                prev_dir = 2;
            }
        }
    else if (check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1, b_2 + h)))){
        if ((check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1, b_2 + 3*h)))) && prev_dir === 3){
            b_2 += h*3;
        }
        else {
            b_2 += h;
            prev_dir = 3;
        }
    }
    else if (check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1, b_2 - h)))){
        if ((check_func.direction(CF, CF_func.CFU(mine_func.minecraft(a_1, b_2 - 3*h)))) && prev_dir === 4){
            b_2 -= h*3;
        }
        else {
            b_2 -= h;
            prev_dir = 4;
        }
    }
    else break;
    i++;
    a_1_Point.push(+a_1.toFixed(3));
    b_2_Point.push(+b_2.toFixed(3));
}
let F = (mine_func.minecraft(a_1_Point[a_1_Point.length-1], b_2_Point[b_2_Point.length-1]));
//F = +F.toFixed(3);
let str = JSON.stringify(F, null, 1);

fs.writeFile('./mas.txt', str, function (error){
  if (error) throw error;
  console.log('Асинхронная запись файла завершена');
});

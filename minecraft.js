'use strict';
function mine() {
    this.minecraft = function (a_1, b_2) {
        this.a_1 = a_1;
        this.b_2 = b_2;
        let h = 0.1, y, t, b_1 = 2, b_3 = 2, k = 2, x = 9;
        let Z = new Array(3), Dz = new Array(3), y_mas = [];
        let res;
        for (t = 0; t < 3; t++) {
            Z[t] = 0;
        }
        for (t = 0; t < 300; t++) {
            Dz[0] = Z[0] + h * Z[1];
            Dz[1] = Z[1] + h * Z[2];
            Dz[2] = Z[2] + h * ((x - Z[0] - (b_1 + this.b_2) * Z[1] - (b_3 + b_1 * this.b_2) * Z[2]) / (b_1 * b_3));
            y = k * (Z[0] - 2 * this.a_1 * Z[1] + this.a_1**2* Z[2]);

            y_mas.push(y);

            Z[0] = Dz[0];
            Z[1] = Dz[1];
            Z[2] = Dz[2];
        }
        res = y_mas;
        return(res);
    }
}
module.exports = mine;
'use strict';
let math = require('mathjs');
let fs = require('fs');

function CF() { // функция подсчета целевой функции
    /**
     * @return {number}
     */
    this.CFU = function (A) {
        this.A = A;
        let yExp = [];
        let res, sum = 0, i;
        let temp = fs.readFileSync('./y.txt').toString().split("\n");
        for (i = 0; i < temp.length; i++) {
            yExp.push(parseFloat(temp[i]));
        }
        for (i = 0; i < yExp.length; i++) {
            sum += math.abs(yExp[i] - this.A[i]);
        }
        res = sum;
        return res;

    }
}
module.exports = CF;